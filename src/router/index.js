import { createRouter, createWebHistory } from "vue-router";

const home = () => import("../pages/Home.vue");
const account = () => import("../pages/Account.vue");

const router = createRouter({
    routes: [
        { path: "/", component: home },
        { path: "/account", component: account },
    ],
    history: createWebHistory(),
});

export default router;
