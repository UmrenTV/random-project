import { createStore } from 'vuex';


const store = createStore({
    state: {
        counter: 0,
    },
    mutations: {
        changeCounter(state, payload) {
            state.counter = payload;
        },
        increaseByOne(state) {
            state.counter++;
        },
    },
});

export default store;
